var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function (key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/", "")
        .replace("?format=json", "")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/", "_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function (err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function () {

    describe('Origins', function () {
        var collection = db.collections.Origins;

        it('should fetch the origin from the remote API', function (done) {
            collection.fetch({
                success: function (collection) {
                    collection.length.should.equal(10);
                    done();
                }
            });
        });

        it('should fetch the next page from the remote API', function (done) {
            collection.fetchNextPage({
                success: function (collection) {
                    collection.length.should.equal(0);
                    done();
                }
            });
        });

        it('should return custom page sizes', function (done) {
            collection.pageSize = 10;
            collection.fetch({
                success: function (collection) {
                    collection.length.should.equal(10);
                    done();
                }
            });
        });

        it('should return load next page of custom size', function (done) {
            collection.pageSize = 10;
            collection.fetchNextPage({
                success: function (collection) {
                    collection.length.should.equal(0);
                    done();
                }
            });
        });

        it('should create models for all the elements in the response', function () {
            collection.fetch({
                success: function (collection) {
                    collection.at(0).should.equal(collection.get(1));
                    collection.at(9).should.equal(collection.get(10));
                    collection.pluck("name").join(",").should.equal('Mutant,Cyborg,Alien,Human,Robot,Radiation,God/Eternal,Animal,Other,Infection');
                }
            });

        });

        it('should expose origin attributes', function () {
            collection.fetch({
                success: function (collection) {
                    var origin = collection.get(4);
                    origin.get("id").should.equal(4);
                    origin.get("name").should.equal("Human");
                    origin.get("api_detail_url").should.equal("http://api.comicvine.com/origin/4/");
                    origin.get("site_detail_url").should.equal("http://www.comicvine.com/human/11-4/");
                }
            });
        });
    });
});