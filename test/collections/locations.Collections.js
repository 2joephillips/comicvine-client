var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function(key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/","")
        .replace("?format=json","")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/","_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function(err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function(){

    describe('Locations', function(){
        var collection = db.collections.Locations;

        it('should fetch the issues from the remote API', function(done) {
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should fetch the next page from the remote API', function(done) {
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should return custom page sizes', function(done) {
            collection.pageSize=10;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should return load next page of custom size', function(done) {
            collection.pageSize=10;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(10);

                done();
            }});
        });

        it('should create models for all the elements in the response', function() {
            collection.at(0).should.equal(collection.get(40910));
            collection.at(9).should.equal(collection.get(41038));
            collection.pluck("name").join(",").should.equal('Khera,Keystone City,Genosha,Asgard,Science City Zero,Wakanda,Atlantis,Latveria,Avengers Mansion,Zenn-La');
        });

        it('should expose location attributes', function() {
            collection.fetch({success: function(collection) {
                var location = collection.get(23611);

                location.get("id").should.equal(23611);
                location.get("description").should.equal("<h2>History</h2><p>Gotham City is one of the oldest Eastern urban centers in the US. It nestles at the mouth of the turbid Gotham River upon islands once peopled by the vanished Miagani tribe of</p><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"1870482\"><div class=\"wiki-img-right\"><div class=\"wiki-img-medium\"><a href=\"http://media.comicvine.com/uploads/3/31566/1870482-all_star_western_1_super.jpg\" title=\"\"><img id=\"1870482\" src=\"http://media.comicvine.com/uploads/3/31566/1870482-all_star_western_1_medium.jpg\" alt=\"\" /></a><div class=\"item-caption\"></div></div></div></div><p>Native Americans. A Norwegian mercenary founded Gotham City in 1635, but the British later took it over. It was also the site for a major battle in the American Revolution.</p><p>There is also a story that 17th-century Gotham Village\u00b4s first dwelling was, in fact a asylum that predated city\u00b4s infamous Arkham Asylum. A man called Hiram was first building a chapel but his partner in crime of murder insisted it to became a sanatorium.</p><p>Gotham\u00b4s 19th-century patrons envisioned their community as a concrete and steel stronghold for pious righteousness and booming industrial growth, and for many generations the wealth and business ventures of Wayne family bolstered the city\u00b4s economy, but it is best known for it\u00b4s soaring crime rate, urban legends and Gothic spires.</p><p><b>Geography</b></p><p>Gotham City\u2019s actual location was never fully explained. The location would vary depending on the writer. A majority of writers usually put Gotham City on the eastern coast on the shores of Lake Gotham. There have been maps created for Gotham but it has also varied many times. Sometimes Gotham City is based on the actual eastern coast and sometimes it would reside around Manhattan or Vancouver.</p><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"159759\"><div class=\"wiki-img-left\"><div class=\"wiki-img-medium\"><a href=\"http://media.comicvine.com/uploads/0/2532/159759-63892-gotham-city_super.jpg\" title=\"\"><img id=\"159759\" src=\"http://media.comicvine.com/uploads/0/2532/159759-63892-gotham-city_medium.jpg\" alt=\"\" /></a><div class=\"item-caption\"></div></div></div></div><p>During the <a href=\"http://www.comicvine.com/no-mans-land/39-42106/\">No Man\u2019s Land</a> story arc, it showed a map of Gotham and it seems to be based on Rhode Island\u2019s geography. Sometimes it has been shown to be a completely original eastern coast and would tie Gotham in with Metropolis. In this version, Gotham is placed on opposite sides of a large bay. It is also located right next to <a href=\"http://www.comicvine.com/bludhaven/34-55771/\">Bludhaven</a>. The distance between Gotham City and <a href=\"http://www.comicvine.com/metropolis/34-41184/\">Metropolis</a> was never fully stated so therefore it would vary over the years. It has been as close as a neighboring city, to as far as several hundreds of miles away. When the Atlas of the DC universe was finally published in the 90s, it revealed that Gotham City was south of <a href=\"http://www.comicvine.com/new-jersey/34-55712/\">New Jersey</a> and Metropolis. However, various references in the comic books indicate that it's somewhere in New Jersey it has other references saying that it is in <a href=\"http://www.comicvine.com/new-york-city/34-41183/\">New York</a>.</p><p>According to DC legend Neal Adams, Gotham's real world counterparts are <a href=\"/new-york-city/34-41183/\">New York City</a> and <a href=\"/chicago-illinois/34-55728/\">Chicago</a>.</p><h2>Architecture</h2><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"86315\"><div class=\"wiki-img-right\"><div class=\"wiki-img-medium\"><a href=\"http://media.comicvine.com/uploads/0/669/86315-18024-gotham-city_super.jpg\" title=\"\"><img id=\"86315\" src=\"http://media.comicvine.com/uploads/0/669/86315-18024-gotham-city_medium.jpg\" alt=\"\" /></a><div class=\"item-caption\"></div></div></div></div><p>Gotham City was Commissioned by Judge <a href=\"/solomon-wayne/29-77680/\">Solomon Wayne</a> to <a href=\"/cyrus-pinkney/29-77681/\">Cyrus Pinkney</a>. Solomon Wayne saw Pinkney's Plans and decided to promote his art.</p><p>Throughout the years, the architectural appearance of Gotham City has varied greatly. Currently, Gotham is portrayed as having a mix of Art Deco, Neo-Gothic and Post-Modernist styling, characterized by enormous sky-scrapers, high-reaching spires, and the presence of gargoyles and other iconic statuary. A constant factor in the depiction of</p><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"159758\"><div class=\"wiki-img-right\"><div class=\"wiki-img-medium\"><a href=\"http://media.comicvine.com/uploads/0/2532/159758-189141-gotham-city_super.jpg\" title=\"\"><img id=\"159758\" src=\"http://media.comicvine.com/uploads/0/2532/159758-189141-gotham-city_medium.jpg\" alt=\"\" /></a><div class=\"item-caption\"></div></div></div></div><p>Gotham City is element of urban decay, which plagues the city even after it's reconstruction.</p><p>In all rendition, regardless of creator, Gotham's architecture acts as a major literary device, used to set the atmosphere and tone. Writer Dennis O'Neill has described Gotham, figuratively, as \"... Manhattan below Fourteenth Street at eleven minutes past midnight on the coldest night in November.\"</p><h2>Notable Residents</h2><p>Batman was not actually the first DC hero in Gotham. During the Golden Age, <a href=\"http://www.comicvine.com/alan-scott/29-12663/\">Alan Scott</a> (Green Lantern) and the Golden Age <a href=\"http://www.comicvine.com/black-canary/29-1689/\">Black Canary</a> lived in Gotham.</p><p>Gotham City is the city where the crime fighters <a href=\"http://www.comicvine.com/batman/29-1699/\">Batman</a> and <a href=\"http://www.comicvine.com/dick-grayson/29-1691/\">Dick Grayson</a> operate. Eventually the <a href=\"http://www.comicvine.com/batman-family/65-45515/\">Batman Family</a> would grow and include <a href=\"http://www.comicvine.com/tim-drake/29-9290/\">Tim Drake</a>, <a href=\"http://www.comicvine.com/oracle/29-5368/\">Barbara Gordon</a>, <a href=\"http://www.comicvine.com/huntress/29-1690/\">Huntress</a>, <a href=\"http://www.comicvine.com/batgirl/29-1701/\">Batgirl</a> and <a href=\"http://www.comicvine.com/batwoman/29-9052/\">Batwoman</a>.</p><p>Though Batman is the first superhero that comes to mind when Gotham City is raised, many other DC characters are known to be living in Gotham City: <a href=\"http://www.comicvine.com/plastic-man/29-6270/\">Plastic Man</a>, <a href=\"http://www.comicvine.com/zatanna/29-5691/\">Zatanna</a>, <a href=\"http://www.comicvine.com/zatara/29-19190/\">Zatara</a>, <a href=\"http://www.comicvine.com/question/29-3211/\">The Question</a>, <a href=\"http://www.comicvine.com/creeper/29-5713/\">The Creeper</a>, and <a href=\"http://www.comicvine.com/simon-dark/29-47794/\">Simon Dark</a> . The <a href=\"http://www.comicvine.com/justice-society-of-america/65-27589/\">Justice Society of America</a> has also been shown to operate in Gotham City.</p><p>Also many important criminal reside in Gotham such as the <a href=\"http://www.comicvine.com/joker/29-1702/\">Joker</a>, <a href=\"http://www.comicvine.com/harley-quinn/29-1696/\">Harley Quinn</a>, <a href=\"http://www.comicvine.com/killer-croc/29-3725/\">Killer Croc</a>, <a href=\"http://www.comicvine.com/riddler/29-3718/\">the Riddler</a>, <a href=\"http://www.comicvine.com/two-face/29-5555/\">Two-Face</a>, <a href=\"http://www.comicvine.com/penguin/29-4885/\">the Penguin</a> and many others.</p><p>Cyrus Gold was wealthy Gotham gentleman and philanthropist who was killed in 'Slaughter Swap', and later resurrected as the monstrous villain <a href=\"/solomon-grundy/29-8342/\">Solomon Grundy</a>. Grundy typically resides in Gotham's sewer system and has been an inconvenient, and often reluctant, enemy of Batman.</p><h2>Important places</h2><ul><li><a href=\"http://www.comicvine.com/arkham-asylum/34-31682/\">Arkham Asylum</a></li><li><a href=\"/wayne-manor/34-55767/\">Wayne Manor</a></li><li><a href=\"http://www.comicvine.com/batcave/34-31340/\">Batcave</a></li><li><a href=\"http://www.comicvine.com/batbunker/34-56723/\">Bat-Bunker</a></li><li><a href=\"http://www.comicvine.com/the-iceberg-lounge/34-56813/\">Iceberg Lounge</a></li><li><a href=\"http://www.comicvine.com/crime-alley/34-55715/\">Crime Alley</a></li><li><a href=\"/ace-chemical-processing-plant/34-57342/\">Ace Chemicals</a></li><li><a href=\"/wayne-enterprises/34-56461/\">Wayne Enterprises</a></li><li><a href=\"http://www.comicvine.com/blackgate-penitentiary/34-56051/\">Blackgate Penitentiary</a></li><li><a href=\"/old-wayne-tower/34-57625/\">Old Wayne Tower</a></li><li><a href=\"/robinson-park/34-57125/\">Robinson Park</a></li></ul><h2>Notable Neighborhoods</h2><h3>Gotham Heights</h3><p>Gotham Heights is an upper-middle class neighborhood in Gotham City, and was under the protection of a teenaged Tim Drake as Robin. The neighborhood is markedly nicer than most areas of the city with little sign of urban decay and poverty.</p><p><b>The Bowery</b></p><p>One of Gotham City\u00b4s worst neighborhoods. It is located to north of Crime Alley and includes Crown Point, a smaller inner-district ridden with crime, homelessness and prostitution.</p><p><b>The Cauldron</b></p><p>An area known for organized crime, it is run by the Irish Mob.</p><h3>Chinatown</h3><p>Gotham City\u00b4s Asian neighborhood.</p><h3>Diamond District</h3><h3><b>Fashion District</b></h3><h3><b>Financial District</b></h3><h3><b>The East End</b></h3><p><b>An underdeveloped part of Gotham troubled with crime, drugs, poverty and prostitution.</b></p><h3><b>Gotham Docks</b></h3><p><b>Gotham City\u00b4s harbour.</b></p><h3><b>Gotham Village</b></h3><p><b>Bohemian area of the city. Similar to New York City\u00b4s Greenwich Village.</b></p><h3><b>Old Gotham</b></h3><p>Old part of the city where GCPD headquarters are located.</p><h3>Toxic Acres</h3><p>Abandoned neighborhood of newly built houses, but unsuitable for habitation due to closeness of a toxic waste dump.</p><h2>Organized Crime</h2><h3><b>Falcone Crime Family (Italian)</b></h3><p><b>Run by <a href=\"/carmine-falcone/29-42288/\">Carmine \"The Roman\" Falcone</a> , and after his death by his daughter <a href=\"/sofia-falcone-gigante/29-22812/\">Sofia Gigante</a> until her death.</b></p><h3><b>Galante Crime Family (Italian)</b></h3><p>Control\u00b4s the East side of Gotham.</p><h3><b>Maroni Crime Family (Italian)</b></h3><p><b>Run by <a href=\"/luigi-maroni/29-53824/\">Luigi \"Big Lou\" Maroni</a> and after his death by his son <a href=\"/salvatore-maroni/29-42582/\">Sal Maroni</a> who was responsible for scarring Harvey Dent.</b></p><h3><b>Odessa Crime Family (Ukrainian)</b></h3><p>Arms dealers.</p><h3><b>Riley Crime Family (Irish)</b></h3><p>Run by <a href=\"/ventriloquist/29-47655/\">Peyton Riley</a>'s father Sean Riley until his death.</p><h3><b>Sabatino Crime Family (Italian)</b></h3><p>The first crime family of Gotham.</p><h3><b>Dimitrov Crime Family (Russian)</b></h3><p>Run by Yuri Dimitrov. Arch enemies of the Maroni Crime Family.</p><h2><b>Bars and Nightclubs</b></h2><h3>Finnigan\u00b4s</h3><p>A bar frequented by the uniformed cops.</p><h3>The Iceberg Lounge</h3><p>Fancy nightclub located in downtown Gotham and operated by the Penguin.</p><h3>My Alibi</h3><p>An underworld bar.</p><h3>The Stacked Deck</h3><p>An underworld nightclub.</p><h2>Sports Teams</h2><p>Football: Gotham Wildcats, Gotham Knights, Gotham Rogues</p><p>Basketball: Gotham Guardsmen</p><p>Baseball: Gotham Knights, Gotham Griffins</p><p>Ice Hockey: The Gotham Blades</p><p><b>The Location</b></p><p>The actual location of Gotham City is said to be in New York, along the eastern coast of the state.</p><h3>Media</h3><p>Gotham City is also seeing in the Batman Animated Series in the 1992 Batman Series up to 1999 and in 2001-2006 the Justice League Animated Series and also in Batman:The Brave and the Bold in 2008.In the Teen Titans Animated Series from 2003 to 2006. <a href=\"http://www.comicvine.com/beast-boy/29-3586/\">Beast Boy</a> and the <a href=\"http://www.comicvine.com/teen-titans/65-19081/\">Teen Titans</a> where playing football in the park and Beast Boy said while playing the game 1 Gotham City 2 Gotham City 3 Gotham City.<a href=\"http://www.comicvine.com/terra/29-5307/\">Terra</a> said to Beast Boy when they were going out to have some pie at a bar. She told Beast Boy that she just moved to LA from Gotham City.<a href=\"http://www.comicvine.com/cyborg/29-2388/\">Cyborg</a> said to the Titans that he was punched and got thrown half way to Gotham City.</p><h3>Films</h3><p>Gotham appears in every Batman film.</p>");
                location.get("deck").should.equal("Gotham City is under the protection of Batman and his proteges from the criminally insane supervillains, gangs, and mobs that litter its streets.");
                location.get("api_detail_url").should.equal("http://api.comicvine.com/location/23611/");
                location.get("site_detail_url").should.equal("http://www.comicvine.com/gotham-city/34-23611/");
                location.get("first_appeared_in_issue").should.be.instanceOf(db.collections.Issues.model);
                location.get("first_appeared_in_issue").get("name").should.equal("The Case of the Chemical Syndicate");
                location.get("aliases").should.equal("");
                location.get("count_of_issue_appearances").should.equal(4103);
                location.get("date_last_updated").should.equal(new Date("2012-08-25 02:42:58"));
                location.get("date_added").should.equal(new Date("2008-06-06 11:27:49"));
                location.get("name").should.equal("Gotham City");
            }});
        });
    });
});