var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function(key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/","")
        .replace("?format=json","")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/","_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function(err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function(){

    describe('Powers', function(){
        var collection = db.collections.Powers;

        it('should fetch the powers from the remote API', function(done) {
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should fetch the next page from the remote API', function(done) {
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(30);
                done();
            }});
        });

        it('should return custom page sizes', function(done) {
            collection.pageSize=10;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should return load next page of custom size', function(done) {
            collection.pageSize=10;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should create models for all the elements in the response', function() {
            collection.at(0).should.equal(collection.get(11));
            collection.at(9).should.equal(collection.get(20));
            collection.pluck("name").join(",").should.equal('Radar Sense,Psychic,Force Field,Blast Power,Healing,Magic,Weapon Master,Super Sight,Super Smell,Super Hearing');
        });

        it('should expose power attributes', function() {
            var model = collection.get(20);

            model.get("id").should.equal(20);
            model.get("name").should.equal("Super Hearing");
            model.get("api_detail_url").should.equal("http://api.comicvine.com/power/20/");
            model.get("site_detail_url").should.equal("http://www.comicvine.com/super-hearing/41-20/");
            model.get("date_added").should.eql(new Date("2008-06-06 11:28:15"));
            model.get("date_last_updated").should.eql(new Date("2008-06-06 11:28:15"));
            model.get("description").should.equal("");
            model.get("aliases").should.equal("");
        });
    });
});