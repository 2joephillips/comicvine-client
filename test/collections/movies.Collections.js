var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function(key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/","")
        .replace("?format=json","")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/","_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function(err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function(){

    describe('Movies', function(){
        var collection = db.collections.Movies;

        it('should fetch the movies from the remote API', function(done) {
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should fetch the next page from the remote API', function(done) {
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should return custom page sizes', function(done) {
            collection.pageSize=10;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should return load next page of custom size', function(done) {
            collection.pageSize=10;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should create models for all the elements in the response', function() {
            collection.at(0).should.equal(collection.get(11));
            collection.at(9).should.equal(collection.get(20));
            collection.pluck("name").join(",").should.equal('Superman Returns,Batman Begins,V for Vendetta,Sin City,The Dark Knight,300,Iron Man,Ghost Rider,The Rocketeer,30 Days of Night');
        });

        it('should expose movie attributes', function() {
            collection.fetch({success: function(collection) {
                var movie = collection.get(12);

                movie.get("id").should.equal(12);
                movie.get("rating").should.equal(3);
                movie.get("date_last_updated").should.equal(new Date("2012-08-11 23:09:05"));
                movie.get("description").should.equal("<p>The movie starts off in a flashback. Bruce was still a young child, playing in the backyard of his home with Rachel Dawes. Bruce falls into a well and into a cave, where he is attacked by a horde of bats. His dad eventually gets him out and takes him into the house, and then Bruce awakens from the dream. He is a young adult in a foreign prison, where he fights criminals one by one. Eventually Bruce meets a man named Henri Ducard, who offers him a chance to join the League of Shadows as long as he gathers a rare flower. Bruce succeeds and climbs the mountain up to Henri Ducard. After he arrives, he meets <a href=\"/ras-al-ghul/29-40816/\">Ra\u2019s al Ghul</a> and tells Ducard about how his parent</p><p>died and the guilt and anger that he carries. Ducard begins training Bruce the way of the ninja. During the training, Bruce tells Ducard about the reason he did not avenge his parents during the trial of Joe Chill (killer of the Waynes and also the same killer in the comics), and then how he became a criminal in order to learn how they think, which is how Bruce ended up in prison in the first place. Bruce completes his final test; however, he has to execute a criminal in order to become a full member. But Bruce does not want to become an executioner. He instead flings a hot brand onto the explosives, detonating them all. Bruce fights Ra\u2019s Al Ghul until the roof collapses onto the latter. Bruce then saves Ducard from falling off the cliff, then he returns to Gotham via private jet with Alfred. Back in Gotham, Bruce discovers a huge cave underneath the mansion. Later, Bruce shows up at Wayne Enterprises, surprising all the executives because Bruce had been declared dead years ago.</p><p>Bruce asks for a job at the applied sciences department. Heading over there, he discovers the vast amount of technology there. He meets Lucius Fox (Morgan Freeman) and gets a tour. Bruce asks to \u201cborrow\u201d the suit for cave diving. He then continues working on the cave, installing lights and then discovering more areas in the cave. Bruce then began ordering parts from dummy corporations to build himself a Bat costume. Later that night, Bruce goes out with the armor and a ski mask. He sneaks up on Officer James Gordon, and tells him about the plan to put criminals behind bars. He tells him to look for his sign.</p><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"2128354\"><div class=\"wiki-img-right\"><div class=\"wiki-img-medium\"><a href=\"http://media.comicvine.com/uploads/10/100264/2128354-batmanbeginsprofpic_super.jpg\" title=\"\"><img id=\"2128354\" src=\"http://media.comicvine.com/uploads/10/100264/2128354-batmanbeginsprofpic_medium.jpg\" alt=\"\" /></a><div class=\"item-caption\"></div></div></div></div><p>Bruce eventually gets all the equipment needed to make his Bat Suit and on his first night out on patrol he encounters Carmine Falcone and his goons exporting /importing drugs with a crooked cop. Once he takes care of all Falcone's men he goes after Falcone after asking who he is he simply says \"I'm Batman.\" Afterward Gordon arrives seeing Falcone's men, but doesn't think anything of it because there is no connection to Falcone. However he learns that Falcone is on the grounds tied up to a Flood light and when Gordon looks out to see how it shines we get our first crude Bat Signal.</p><p>After Bruce''s first night out on the job, Alfred notices the bruises and advises Bruce to get an alibi for all his injuries, he suggests Polo. Dr. Crane goes to see Falcone in prison, who wants to be transferred to Arkham. We learn that Crane is working for someone and shows <a href=\"/carmine-falcone/29-42288/\">Falcone</a> the scarecrow mask. Bruce goes out to insure his Public identity as Bruce Wayne, coming across as a spoiled playboy, buying a hotel after his dates start swimming in a pool. He later goes on Patrol and finds Crane's hideout, however he is affected by the fear gas and has to flee, calling Alfred for help.</p><p>Days go by and eventually Bruce recovers, Alfred tells him Lucius Fox helped supply an antidote for the weaponized hallucinogen. Bruce asks Fox to make some more. Rachel stops by and drops off his birthday present before having to go to the Narrows to talk to Crane. Bruce rushes to his cave telling Alfred to entertain his guests until he returns.</p><p>At Arkham when Rachel asks too many questions Crane is forced to give a concentrated dose of his fear Toxin. Batman arrives and having been immunized against the toxin, uses it on Crane, who talks. He says Ra's Al Ghul is the person behind everything, including adding the toxin to the water supply. Before Batman can get anything more out of him the police arrive. The cops want to wait for SWAT but Gordon goes in anyway. Batman grabs him explaining the situation and asks him to take her and meet him out back. Once there Batman takes her to the Bat Cave, although making some additions to the nightly news on the way. When he releases Rachel from the Toxin he gives her a sedative and goes upstairs to the party. Alfred tells him he needs to be careful and not show off to which Bruce replies Rachel is downstairs, take her home. As Bruce enters the party he goes to Fox, who tells him he was just fired, but Bruce wants him back at Wayne Enterprises to make more Vaccines for the Toxin. As Bruce tries to make his way around the party he is introduced to Ra's Al Ghul, who he believes to be dead, however it turns out that Ra's is actually Ducard. Ra's tells him to either get the people out of the house or he will make a scene. Bruce takes this time to become \"drunk\" and tells his guests to get the hell out. As the guest leave Bruce sees all the Assassins in the room. Ra's tells him his plans of destroying Gotham and wants Bruce to join him. He also learns that this isn't the first time Gotham has been targeted for destruction. Bruce tells him Gotham isn't beyond saving. Ra's gives the order to his men to start burning his house, as Ra's tells him he never minded his surroundings as a beam falls on Bruce. \"You burned my house and left me for dead. Consider us even.\" As Wayne Manor is burning Alfred returns and finds Bruce helping him down to the cave.</p><p>Bruce is distraught, everything his father built is burning because of him. Alfred sees that Bruce is bleeding but tells him \"Why do we fall, Sir? So we can learn to pick ourselves back up.\" A line his father told him when he was little. As Bruce heads out to save Gotham, Rachel goes to the Narrows to give Gordon his anti-Toxin. As the bridges close and the gas released Gordon is the only one left and calls for back-up. Commissioner Loeb tells him there is no one left, as Batman, in the <a href=\"../../batmobile/18-41463/\" rel=\"nofollow\">Batmobile</a> jumps the gap. He has Gordon go to Wayne Tower to try to Blow up the tracks as he goes to fight Ra's. Along the way, he saves Rachel, who asks who he is. He responds \"It's not who I am underneath, but what I do that defines me.\" Something she told Bruce Wayne earlier and realizes who Batman really is. As Bruce begins to fight Ra's on the train, Ra's believes he's trying to stop the train, but instead he insures that the train doesn't stop. As Gordon blows up the tracks he tells Ra's he never did mind his own surroundings. Ra's believes Bruce is going to kill him to which he replies \"I won't kill you, but I don't have to save you.\" As he leaves Ra's on the train as it crashes.</p><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"2128359\"><div class=\"wiki-img-center\"><div class=\"wiki-img-super\"><a href=\"http://media.comicvine.com/uploads/10/100264/2128359-batman_begins_1_1_.jpg\" title=\"\"><img id=\"2128359\" src=\"http://media.comicvine.com/uploads/10/100264/2128359-batman_begins_1_1__super.jpg\" alt=\"\" /></a><div class=\"item-caption\"></div></div></div></div><p>The following day Bruce hires Fox as CEO and fires the previous one. Heading back to Wayne Manor he talks to Rachel who tells him, if one day Gotham doesn't need Batman she will be there waiting for him. When asked what he will do with the Manor he replies \"Rebuild it, Brick by Brick\" exactly the way it was. Alfred tells him they could do something in the southeast corner, referring to the <a href=\"../../batcave/34-31340/\" rel=\"nofollow\">Bat-cave</a>.</p><p>Jim Gordon has been promoted and signals Batman with a <a href=\"../../bat-signal/18-55819/\" rel=\"nofollow\">Bat Signal</a>, joking that he couldn't find any mob bosses. He then questions about the escalation that is going to happen because of Batman. He gives Batman a case. Double Homicide and leaves a calling card. It is revealed to be a Joker card. Batman says he'll look into it. Gordon says he never got to thank him, to which Batman replies \"and You'll never have to.\" Batman jumps off the building and circles around as the movie ends.</p><h3>Comic Book Adaptations</h3><p>1) <a href=\"/batman-begins-the-movie-and-other-tales-of-the-dark-knight/49-31294/\">Batman Begins: The Movie and Other Tales of the Dark Knight</a></p><p>2) <a href=\"/batman-begins-official-movie-adaptation/49-44587/\">Batman Begins: Official Movie Adaptation</a></p>");
                movie.get("deck").should.equal("The first in a \"re-booted\" series of Batman films. Batman Begins takes a darker, edgier tone than past films. Batman is portrayed by Christian Bale.");
                movie.get("studios").should.be.an.instanceof(Array);
                movie.get("studios").join(",").should.equal("DC Comics,Syncopy Films,Legendary Pictures");
                movie.get("box_office_revenue").should.equal("205,343,774");
                movie.get("director").should.equal("Christopher Nolan");
                movie.get("total_revenue").should.equal("372,710,015");
                movie.get("writers").should.be.an.instanceof(Array);
                movie.get("writers").join(",").should.equal("Christopher Nolan,David Goyer");
                movie.get("producers").should.be.an.instanceof(Array);
                movie.get("producers").join(",").should.equal("Emma Thomas,Larry Franco,Charles Roven,Lorne Orleans");
                movie.get("budget").should.equal("150 million");
                movie.get("distributor").should.equal("Warner Bros.");
                movie.get("date_added").should.equal(new Date("2009-07-08 11:58:43"));
                movie.get("site_detail_url").should.equal("http://www.comicvine.com/batman-begins/223-12/");
                movie.get("runtime").should.equal(140);
                movie.get("api_detail_url").should.equal("http://api.comicvine.com/movie/12/");
                movie.get("release_date").should.equal(new Date("2005-06-14 00:00:00"));
                movie.get("name").should.equal("Batman Begins");
            }});
        });
    });
});