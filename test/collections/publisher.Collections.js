var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function(key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/","")
        .replace("?format=json","")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/","_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function(err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function(){

    describe('Publishers', function(){
        var collection = db.collections.Publishers;

        it('should fetch the publishers from the remote API', function(done) {
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should fetch the next page from the remote API', function(done) {
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should return custom page sizes', function(done) {
            collection.pageSize=10;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should return load next page of custom size', function(done) {
            collection.pageSize=10;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should create models for all the elements in the response', function() {
            collection.at(0).should.equal(collection.get(22));
            collection.at(9).should.equal(collection.get(52));
            collection.pluck("name").join(",").should.equal('Eastern Color,National Periodical Publications,K,Marvel,Ace Magazines,Fawcett Publications,Hillman,Prize,Street And Smith,Lev Gleason');
        });

        it('should expose publisher attributes', function() {
            collection.fetch({success: function(collection) {
                var publisher = collection.get(10);
                publisher.get("id").should.equal(10);
                publisher.get("location_city").should.equal("");
                publisher.get("location_state").should.equal("");
                publisher.get("date_last_updated").should.equal(new Date("2012-08-15 20:31:08"));
                publisher.get("description").should.equal("<h2>History</h2><p>DC was founded in 1934 under the name of National Allied Publications. DC was formed by the merging of National Allied Publications and Detective Comics. The Company changed it's name as a result of its nickname: DC Comics. People had nicknamed it DC because of one its most popular comic series; <a href=\"http://www.comicvine.com/detective-comics/49-18058/\">Detective Comics</a> It is perhaps the world's most recognizable comic companies and rivals <a href=\"/marvel/57-31/\">Marvel</a> publishing.</p><p>DC Comics has its official headquarters at 1700 Broadway, 7th, New York, New York. Random House distributes DC Comics' books to the bookstore market, while Diamond Comics Distributors supplies the comics shop specialty market.</p><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"1837076\"><div class=\"wiki-img-right\"><div class=\"wiki-img-medium\"><a href=\"http://media.comicvine.com/uploads/5/58424/1837076-dc_comics_reinvents_pg564mg_x_large_super.jpg\" title=\"Justice League # 1\"><img id=\"1837076\" src=\"http://media.comicvine.com/uploads/5/58424/1837076-dc_comics_reinvents_pg564mg_x_large_medium.jpg\" alt=\"Justice League # 1\" /></a><div class=\"item-caption\">Justice League # 1</div></div></div></div><p>National Allied Publications' first comic was <i>New Fun: The Big Comic Magazine #1</i> which was released in the February of 1935. It hit off, and was surprisingly popular. Later that year, a second title was released: <i>New Comics #1.</i> The size and length <i>New Comics #1</i> became the archetype for many comics afterwards, and it became the longest running comic series of all time.</p><p>With the establishment of DC as the major comic book company and with many of the most marketable names in the early years of comics, it became the main company that other companies tried to compete with. Companies tried different approaches, for instance <a href=\"/fawcett-publications/57-34/\">Fawcett Publications</a> tried to copy the formula of <a href=\"/superman/29-1807/\">Superman</a> and to have an all-powerful hero with a supporting \"family\", whereas other companies such as <a href=\"/charlton/57-125/\">Charlton Comics</a> created entire new worlds. Further along in the <a href=\"/silver-age-of-comics/12-55821/\">Silver Age</a> many of these companies failed and either DC or Marvel bought the rights to the characters. Thus although they had once been competitors the properties of Fawcett and Charlton both eventually came under DC's control.</p><p>With the reinvigoration of Marvel in the 1960s under the leadership of <a href=\"/stan-lee/26-40467/\">Stan Lee</a> and <a href=\"/jack-kirby/26-5614/\">Jack Kirby</a>, DC found itself a new and more potent competitor. Marvel succeeded by breaking what had become by then generic archetypes of superheroes by introducing characters which were younger and more flawed (and thus appeared more human and appealed to a younger crowd in a more direct manner.) After falling behind Marvel in sales, DC was finally forced to adopt much of the same system which Marvel had, by introducing such young teams as the <a href=\"/teen-titans/65-19081/\">Teen Titans</a> to compete with the <a href=\"/x-men/65-3173/\">X-Men</a>.</p><p>With the same concept in mind, in 2011, DC announced they were cancelling all the main titles and starting over with all <a href=\"/the-new-52/12-56061/\">New 52's</a> as part of a relaunch. In charge of the relaunch is <a href=\"/geoff-johns/26-40439/\">Geoff Johns</a> and <a href=\"/jim-lee/26-2399/\">Jim Lee</a>. The relaunch started with the release of <a href=\"http://www.comicvine.com/justice-league-justice-league-part-one/37-290431/\">Justice League # 1</a> on August 31st. this was done partially to revitalize the company and to deage the characters and thus make them more dynamic. On January 12, 2012, it was announced that there would be a second wave of the new 52 where six titles would be getting canceled with another six to replace them, though the plan to stay at 52 ongoing monthlies seems to be consistent.</p><p>DC comics now has 28 live action movies, 16 animated films (in all likely hood more), 42 television series's and 39 video games based on the DC universe. <b>DC Imprints and Sub-Imprints Both Past and Present</b></p><ul><li><a href=\"http://www.comicvine.com/vertigo/57-521/\">Vertigo</a> (1993-Present)</li><li><a href=\"http://www.comicvine.com/wildstorm/57-708/\">WildStorm</a> (1999-2010)*</li><li><a href=\"http://www.comicvine.com/cmx/57-2345/\">CMX</a> (2004-2010)</li><li><a href=\"http://www.comicvine.com/helix/57-602/\">Helix</a> (1996-1998)</li><li><a href=\"http://www.comicvine.com/2000ad/57-2358/\">2000AD</a></li><li><a href=\"http://www.comicvine.com/americas-best-comics/57-731/\">America's Best Comics</a> (1999-2010)</li><li><a href=\"http://www.comicvine.com/cliffhanger/57-2287/\">Cliffhanger</a></li><li><a href=\"http://www.comicvine.com/tangent-comics/57-638/\">Tangent Comics</a> (1997-2008)</li><li><a href=\"http://www.comicvine.com/paradox-press/57-1982/\">Paradox Press</a> (1993-2001)</li><li><a href=\"http://www.comicvine.com/piranha-press/57-2083/\">Piranha Press</a> (1989-1994)</li><li><a href=\"http://www.comicvine.com/humanoids/57-1346/\">Humanoids</a> (2004)</li><li><a href=\"http://www.comicvine.com/milestone/57-538/\">Milestone</a></li><li><a href=\"http://www.comicvine.com/impact/57-483/\">Impact!</a></li><li><a href=\"http://www.comicvine.com/minx/57-2227/\">Minx</a> (2007-2008)</li><li><a href=\"/zuda-comics/57-2069/\">Zuda</a> (2007-2010)</li></ul><p>*the imprint was founded in 1992 but as part of the <a href=\"/image/57-513/\">Image</a> conglomerate and only moved to DC in 1999</p><h2>DC Movies (Cinematic Releases)</h2><ul><li><a href=\"http://www.comicvine.com/batman-the-movie/223-55/\">Batman: The Movie</a> (July 30, 1966)</li><li><a href=\"http://www.comicvine.com/superman/223-2/\">Superman</a> (December 15, 1978)</li><li><a href=\"http://www.comicvine.com/superman-ii/223-5/\">Superman II</a> (June 19, 1981)</li><li><a href=\"http://www.comicvine.com/superman-iii/223-6/\">Superman III</a> (June 17, 1983)</li><li><a href=\"http://www.comicvine.com/supergirl/223-171/\">Supergirl</a> (November 21, 1984)</li><li><a href=\"http://www.comicvine.com/superman-iv-the-quest-for-peace/223-9/\">Superman IV: The Quest For Peace</a> (July 24, 1987)</li><li><a href=\"http://www.comicvine.com/batman/223-4/\">Batman</a> (June 23, 1989)</li><li><a href=\"http://www.comicvine.com/batman-returns/223-7/\">Batman Returns</a> (June 19, 1992)</li><li><a href=\"http://www.comicvine.com/batman-forever/223-8/\">Batman Forever</a> (June 16, 1995)</li><li><a href=\"http://www.comicvine.com/batman-robin/223-10/\">Batman &amp; Robin</a> (June 20, 1997)</li><li><a href=\"http://www.comicvine.com/league-of-extraordinary-gentlemen/223-28/\">League of Extraordinary Gentlemen</a> (June 11, 2003)</li><li><a href=\"http://www.comicvine.com/constantine/223-26/\">Constantine</a> (February 18, 2005)</li><li><a href=\"http://www.comicvine.com/batman-begins/223-12/\">Batman Begins</a> (June 14, 2005)</li><li><a href=\"http://www.comicvine.com/superman-returns/223-11/\">Superman Returns</a> (June 28, 2006)</li><li><a href=\"http://www.comicvine.com/the-dark-knight/223-15/\">The Dark Knight</a> (July 18, 2008)</li><li><a href=\"http://www.comicvine.com/watchmen/223-1/\">Watchmen</a> (March 19, 2009)</li><li><a href=\"http://www.comicvine.com/green-lantern/223-926/\">Green Lantern</a> (June 17, 2011)</li><li><a href=\"http://www.comicvine.com/the-dark-knight-rises/223-1214/\">The Dark Knight Rises</a> (July 20, 2012)</li></ul><h4>Coming Attractions (Cinematic Releases)</h4><ul><li><a href=\"http://www.comicvine.com/man-of-steel/223-1375/\">Man of Steel</a> (June 14, 2013)</li></ul>");
                publisher.get("deck").should.equal("Publisher of iconic characters and teams such as Superman, Batman, Wonder Woman, the Flash, Green Lantern, the Justice League of America, and the Teen Titans. One of the \"big two\" comic publishers, along with Marvel.");
                publisher.get("site_detail_url").should.equal("http://www.comicvine.com/dc-comics/57-10/");
                publisher.get("api_detail_url").should.equal("http://api.comicvine.com/publisher/10/");
                publisher.get("location_address").should.equal("");
                publisher.get("date_added").should.equal(new Date("2008-06-06 11:08:00"));
                publisher.get("aliases").should.equal("");
                publisher.get("name").should.equal("DC Comics");
            }});
        });
    });
});