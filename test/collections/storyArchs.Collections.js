var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function(key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/","")
        .replace("?format=json","")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/","_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function(err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function(){

    describe('StoryArcs', function(){
        var collection = db.collections.StoryArcs;

        it('should fetch the story arcs from the remote API', function(done) {
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should fetch the next page from the remote API', function(done) {
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should return custom page sizes', function(done) {
            collection.pageSize=10;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should return load next page of custom size', function(done) {
            collection.pageSize=10;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should create story arcs for all the elements in the response', function() {
            collection.at(0).should.equal(collection.get(40752));
            collection.at(9).should.equal(collection.get(40773));
            collection.pluck("name").join(",").should.equal('Age of Apocalypse,Judas Contract,Knightfall,A Lonely Place of Dying,The Janus Directive,Crisis on Infinite Earths,Infinite Crisis,Armageddon 2001,Eclipso: The Darkness Within,Great Darkness Saga');
        });

        it('should expose story arcs attributes', function() {
            collection.fetch({success: function(collection) {
                var model = collection.get(27758);

                model.get("id").should.equal(27758);
                model.get("name").should.equal("Operation Galactic Storm");
                model.get("aliases").should.equal("");
                model.get("date_added").should.equal(new Date("2008-06-06 11:27:52"));
                model.get("count_of_issue_appearances").should.equal(21);
                model.get("api_detail_url").should.equal("http://api.comicvine.com/story_arc/27758/");
                model.get("first_appeared_in_issue").should.be.instanceOf(db.collections.Issues.model);
                model.get("first_appeared_in_issue").get("name").should.equal("Operation: Galactic Storm, Part 1: It Came From Outer Space");
                model.get("first_appeared_in_issue").get("issue_number").should.equal("398.00");
                model.get("site_detail_url").should.equal("http://www.comicvine.com/operation-galactic-storm/39-27758/");
                model.get("deck").should.equal("Operation: Galactic Storm was a 19 part Avengers event. It ran within Avengers, West Coast Avengers, Captain America, Thor, Quasar, Iron Man and Wonder Man comics of the time.");
                model.get("description").should.equal("<h2>Plot Summary</h2><p>As a war between the <a href=\"/kree/65-7588/\">Kree</a> and <a href=\"/shiar/65-55854/\">Shi'ar</a> rages, Earth becomes drawn into the conflict. The Shi'ar <a href=\"/imperial-guard/65-6097/\">Imperial Guard</a> come to steal the technology left by the deceased <a href=\"/captain-marvel/29-1472/\">Captain Marvel</a> while Kree agents try to secure their loose technology, getting the attention of the <a href=\"/avengers/65-3806/\">Avengers</a>. Earth's Mightiest Heroes learn not only of the intergalactic war taking place but that their solar system is being used as a strategic hyperspace stop and it is severely destabilizing their sun.</p><p>The east coast branch of the Avengers unite with the <a href=\"/avengers-west-coast/65-29075/\">Avengers West Coast</a> and divide into three teams. One team remains on Earth while the other two journey to the respective throneworlds of the Shi'ar and Kree to find some way of protecting Earth from this war. These space-bound teams are quickly embroiled in conspiracy within both alien empires. The Shi'ar team expose <a href=\"/skrulls/65-40668/\">Skrull</a> infiltrators deep within the Shi'ar ruling body, and the Kree team become fugitives as the <a href=\"/supreme-intelligence/29-6096/\">Supreme Intelligence</a> rises to rule the Kree once more.</p><p>Even with the conspiracies becoming clear, the war continues with a life of its own. The Shi'ar complete their Nega-Bomb, an unprecedented weapon of mass destruction, and the Avengers are unable to stop <a href=\"/lilandra/29-6144/\">Lilandra</a> from deploying it against the Kree. Its detonation ends the war, killing millions upon millions of Kree.</p><p>The Avengers survive and discover the true architect of all this death is the Supreme Intelligence, who sacrificed countless lives so that the few surviving Kree would break free of their evolutionary dead end thanks to the Nega-Bomb's fallout. This leads to half of the Avengers to decide to hunt down the Supreme Intelligence in the ruins of the Kree Empire and execute him for his crimes, despite <a href=\"/captain-america/29-1442/\">Captain America</a>'s strong objections.</p><p>The Shi'ar soon arrive to occupy the former Kree Empire, and the Avengers return to Earth.</p><h2>Collected Editions</h2><ul><li><a href=\"http://www.comicvine.com/operation-galactic-storm-vol-1/37-278054/\">Operation: Galactic Storm Vol. 1</a></li><li><a href=\"http://www.comicvine.com/operation-galactic-storm-vol-1/37-278054/\">Operation: Galactic Storm Vol. 2</a></li></ul><h2>Related Items</h2><ul><li><a href=\"/blockbusters-of-the-marvel-universe/49-38679/\">Blockbusters of the Marvel Universe</a></li><li><a href=\"http://www.comicvine.com/what-if-what-if-the-avengers-lost-operation-galactic-storm/37-38110/\">What If? #55</a></li><li><a href=\"http://www.comicvine.com/what-if-what-if-avengers-lost-operation-galactic-storm/37-38268/\">What If? #56</a></li></ul><h2>Non-U.S. Editions</h2><ul><li><a href=\"http://www.comicvine.com/los-vengadores-operacion-tormenta-galactica-/37-291414/\">Los Vengadores: Operaci\u00f3n Tormenta Gal\u00e1ctica #1</a></li><li><a href=\"http://www.comicvine.com/los-vengadores-operacion-tormenta-galactica-/37-291415/\">Los Vengadores: Operaci\u00f3n Tormenta Gal\u00e1ctica #2</a></li><li><a href=\"http://www.comicvine.com/los-vengadores-operacion-tormenta-galactica-/37-291416/\">Los Vengadores: Operaci\u00f3n Tormenta Gal\u00e1ctica #3</a></li><li><a href=\"http://www.comicvine.com/marvel-crossover-tempesta-nella-galassia-1/37-315378/\">Marvel Crossover #1</a></li><li><a href=\"http://www.comicvine.com/marvel-crossover-tempesta-nella-galassia-2/37-315381/\">Marvel Crossover #2</a></li><li><a href=\"http://www.comicvine.com/marvel-crossover-tempesta-nella-galassia-3/37-315382/\">Marvel Crossover #3</a></li><li><a href=\"http://www.comicvine.com/marvel-crossover-tempesta-nella-galassia-4/37-315383/\">Marvel Crossover #4</a></li><li><a href=\"http://www.comicvine.com/marvel-crossover-tempesta-nella-galassia-5/37-315384/\">Marvel Crossover #5</a></li></ul>");
                model.get("date_last_updated").should.equal(new Date("2012-07-29 10:51:31"));
                model.get("publisher").should.be.instanceOf(db.collections.Publishers.model);
                model.get("publisher").get("name").should.equal("Marvel");
            }});
            
        });
    });
});