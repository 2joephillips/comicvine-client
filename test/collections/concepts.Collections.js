var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function(key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/","")
        .replace("?format=json","")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/","_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function(err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function(){

    describe('Concepts', function(){
        var collection = db.collections.Concepts;

        it('should fetch the concepts from the remote API', function(done) {
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should fetch the next page from the remote API', function(done) {
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should return custom page sizes', function(done) {
            collection.pageSize=10;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should return load next page of custom size', function(done) {
            collection.pageSize=10;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(10);

                done();
            }});
        });

        it('should create models for all the elements in the response', function() {
            collection.at(0).should.equal(collection.get(41622));
            collection.at(9).should.equal(collection.get(42386));
            collection.pluck("name").join(",").should.equal('Kherubim Technology,Fastball Special,Speed Force,Pym Particles,Mutant Growth Hormone,Legacy Virus,Terrigenesis,Kick,Comics Code Authority,Carbonadium');
        });

        it('should expose concept attributes', function() {
            collection.fetch({success: function(collection) {
                var concept = collection.get(40616);

                concept.get("id").should.equal(40616);
                concept.get("api_detail_url").should.equal("http://api.comicvine.com/concept/40616/");
                concept.get("site_detail_url").should.equal("http://www.comicvine.com/with-great-power-comes-great-responsibility/12-40616/");

                concept.get("name").should.equal("With Great Power Comes Great Responsibility");
                concept.get("count_of_issue_appearances").should.equal(59);
                concept.get("deck").should.equal("\"With great power comes great responsibility\" is a mantra that was passed down to Peter Parker from Uncle Ben during Peter's teenage tribulations.");
                concept.get("description").should.equal("<h2>Origin</h2><p>With great power comes great responsibility, originally With great power there must also come great responsibility was a phrase <a href=\"/uncle-ben/29-3114/\">     Uncle Ben</a> told to <a href=\"/spider-man/29-1443/\">     Spider-Man</a> when Peter was going through teenage angst.</p><p>After Ben's death, the Peter took the time to reflect on himself and the mantra and eventually decided to make it his life's motto. Although the motto is applicable to all stages of life, it is especially relevant to mutants/super-humans.  Keep in mind, when Peter, and many super humans first found they had super powers, they used it for personal gain.</p><p>Yet all super-heroes as we know today have gone through the process, understanding their responsibility to protect and further mankind.<br /><br /></p><h3>List of Issues the Line Has Been Stated</h3><p><b><a href=\"http://www.comicvine.com/silver-surfer-vol-3-galactic-grapplerama/37-96268/\">     Silver Surfer Vol. 3 #108</a> - </b>After his powers are returned, the Surfer realizes that he cannot live a casual life as others do, \"I am the bearer of great power -- and with great power, great responsibility!\"<b><br /></b></p><p><b><a href=\"http://www.comicvine.com/annihilation-nova-safety-in-numbers/37-105477/\">     Annihilation: Nova #3</a></b> - <a href=\"/quasar/29-3319/\">     Quasar</a> tells <a href=\"/nova/29-2105/\">     Nova</a> that he has the Quantum Bands and Nova has the Nova Force...and that he needs Richard's help. He then, thinking of their powers, says \"Someone once told me that with great power must also come great responsibility.\" Nova thinks it sounds like something Captain America would say but Quasar tells him it was actually Spider-Man.</p><p><b><a href=\"http://www.comicvine.com/evil-ernie-youth-gone-wild-the-cure-that-kills/37-127222/\">     Evil Ernie: Youth Gone Wild #2</a></b> - <a href=\"/lady-death/29-1847/\">     Lady Death</a> says to <a href=\"/evil-ernie/29-41579/\">     Evil Ernie</a>: \"With great power, comes great responsibility... to those you love\"</p><p><b><a href=\"http://www.comicvine.com/marvel-knights-spider-man-wild-blue-yonder-part-3/37-119603/\">     Marvel Knights Spider-Man #15</a></b> - <a href=\"/ethan-edwards/29-1776/\">     Ethan Edwards</a>' father sends him on the bus with words \"With great power there must also come, great responsibility\" <br />&nbsp;<br /><b><a href=\"http://www.comicvine.com/the-uncanny-x-men-annual-days-of-future-present-pt-4-you-must-remember-this/37-107610/\">    Uncanny X-Men Annual #14</a> </b>- <a href=\"/wolverine/29-1440/\">   Wolverine</a> explains the concept to <a href=\"/franklin-richards/29-2469/\">  Franklin Richards</a> and <a href=\"/rachel-summers/29-3566/\"> Rachel Summers</a>, enabling them to agree to return home and try to set thiings right.<br /></p>");

                concept.get("first_appeared_in_issue").should.be.instanceOf(db.collections.Issues.model);
                concept.get("first_appeared_in_issue").get("name").should.equal("Spider-man!");
                concept.get("first_appeared_in_issue").get("issue_number").should.equal("15.00");

                concept.get("aliases").should.equal("");
            //@todo Test Start Year
            //@todo Test Aliases
            //@todo Test Image
            }});
        });
    });
});