var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function (key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/", "")
        .replace("?format=json", "")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/", "_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function (err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function () {
    describe('Volumes', function () {
        var collection = db.collections.Volumes;

        it('should fetch the volumes from the remote API', function (done) {
            collection.fetch({
                success: function (collection) {
                    collection.length.should.equal(100);
                    done();
                }
            });
        });

        it('should fetch the next page from the remote API', function (done) {
            collection.fetchNextPage({
                success: function (collection) {
                    collection.length.should.equal(100);
                    done();
                }
            });
        });

        it('should return custom page sizes', function (done) {
            collection.pageSize = 10;
            collection.fetch({
                success: function (collection) {
                    collection.length.should.equal(10);
                    done();
                }
            });
        });

        it('should return load next page of custom size', function (done) {
            collection.pageSize = 10;
            collection.fetchNextPage({
                success: function (collection) {
                    collection.length.should.equal(10);

                    done();
                }
            });
        });

        it('should create volumes for all the elements in the response', function () {
            collection.at(0).should.equal(collection.get(816));
            collection.at(9).should.equal(collection.get(898));
            collection.pluck("name").join(",").should.equal('Planet Comics,The Human Torch,Super-Mystery Comics,Master Comics,Whiz Comics,Sparkler Comics,Rangers of Freedom Comics,Daredevil Comics,Four Favorites,Captain Marvel Adventures');
        });


        it('should expose volumes attributes', function () {
            collection.fetch({
                success: function (collection) {
                    var model = collection.get(796);

                    model.get("id").should.equal(796);
                    model.get("api_detail_url").should.equal("http://api.comicvine.com/volume/796/");
                    model.get("site_detail_url").should.equal("http://www.comicvine.com/batman/49-796/");
                    model.get("aliases").should.equal("");
                    model.get("name").should.equal("Batman");
                    model.get("date_added").should.equal(new Date("2008-06-06 11:08:04"));
                    model.get("date_last_updated").should.equal(new Date("2012-07-27 08:13:41"));
                    model.get("deck").should.equal("");
                    model.get("description").should.equal("<p>The entire run of Batman still stands upon large controversies over who actually came up with some of the characters. <a href=\"/bob-kane/26-19137/\">Bob Kane</a> supposedly came up with the idea for the hero, but it has been suggested that he only came up with a \"Bird-man\" and <a href=\"/bill-finger/26-43591/\">Bill Finger</a> suggested he be a \"Bat-man\". Both creators however, share credit for this character. As for the <a href=\"/joker/29-1702/\">Joker</a>, the first concept sketch was drawn by <a href=\"/jerry-robinson/26-9680/\">Jerry Robinson</a>, but Kane disputed that his input was \"minimal\" suggesting he and Bill came up with the idea. Kane also finagled many legal aspects of Batman related print and media. Every movie and comic reads \"Batman created by Bob Kane\" when it should read \"Batman created by Bob Kane and Bill Finger\". However It is not disputed that Robinson and Finger came up with the character, <a href=\"/dick-grayson/29-1691/\">Robin</a> after \"Robinhood\" . Bob Kane and Sheldon Moldoff co-created numerous bat-creations including <a href=\"/alfred/29-5556/\">Alfred Pennyworth</a>, <a href=\"/poison-ivy/29-1697/\">Poison Ivy,</a> <a href=\"/mr-freeze/29-3715/\">Mr. Freeze</a>, <a href=\"/batwoman/29-9052/\">Batwoman</a>, <a href=\"/batgirl/49-7205/\">Batgirl</a> as well as <a href=\"/bat-mite/29-23170/\">Bat-Mite</a> and <a href=\"/ace-the-bat-hound/29-31302/\">Ace the Bat Hound</a> just to name a few. <a href=\"/bill-finger/26-43591/\">Bill Finger</a> also co-created <a href=\"/penguin/29-4885/\">Penguin</a>, <a href=\"/riddler/29-3718/\">Riddler</a>, and <a href=\"/catwoman/29-1698/\">Catwoman</a>.</p><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"1681131\"><div class=\"wiki-img-right\"><div class=\"wiki-img-thumb\"><a href=\"http://media.comicvine.com/uploads/6/64507/1681131-batman1ad_super.jpg\" title=\"Ad for Batman #1\"><img id=\"1681131\" src=\"http://media.comicvine.com/uploads/6/64507/1681131-batman1ad_thumb.jpg\" alt=\"Ad for Batman #1\" /></a><div class=\"item-caption\">Ad for Batman #1</div></div></div></div><p>One of DC's longest running books showcasing the adventures of the Dark Knight, <a href=\"/batman/29-1699/\">Batman</a>. Through out the years, a majority of Batman's marquee story lines have happened within the pages of this book including, but not limited to the death of the second Robin, <a href=\"/jason-todd/29-6849/\">Jason Todd</a>, and <a href=\"/bane/29-6129/\">Bane</a> breaking Batman's back, leading to <a href=\"/jean-paul-valley/29-12834/\">Jean-Paul Valley</a> to assume the role of Batman for a period of time in the 90s.</p><p>In 2006, super-star writer <a href=\"/grant-morrison/26-40434/\">Grant Morrison</a> took over the book. Morrison wrote stories that introduced readers to Bruce's son <a href=\"/damian-wayne/29-42413/\">Damian Wayne</a> as well as putting Bruce through a series of near fatal trials against <a href=\"/black-glove/65-54825/\">The Black Glove</a> and <a href=\"/doctor-hurt/29-54826/\">Doctor Hurt</a>, leading up to the epic 2008 story lines of <a href=\"/batman-rip/39-55116/\">Batman RIP</a> and <a href=\"/final-crisis/39-51642/\">Final Crisis</a> where Bruce Wayne had apparently died at the hands of the evil god <a href=\"/darkseid/29-2349/\">Darkseid</a>.</p><p>After Bruce's \"death,\" and the <a href=\"/battle-for-the-cowl/39-55808/\">Battle for the Cowl</a>, <a href=\"/dick-grayson/29-1691/\">Dick Grayson</a>, the first Robin, took up the mantle of Batman, continuing to protect Gotham exactly as his mentor had. He even took in Damian as his own Robin. With the exception of two issues set prior to the events of <a href=\"/final-crisis/39-51642/\">Final Crisis</a>, Dick Grayson has been the star of the book since issue #687.</p><p>When the super hero community learned that Bruce Wayne had never actually died, and was sent back in time by Darkseid, Dick Grayson eventually knew that his time as Batman would soon end. But with Bruce's return, and formation of <a href=\"/batman-inc/65-57839/\">Batman Incorporated</a>, Bruce deemed Dick Grayson the Batman of Gotham City, as he traveled the world, searching for more recruits for his new organization.</p><p><a href=\"/charles-paris/26-38494/\">Charles Paris</a> has drawn the most issues of Batman thus far at 128. <a href=\"/dennis-oneil/26-40861/\">Denny O'Neil</a> is a long time writer and editor at DC with 211 issues. He also contributed the location of <a href=\"/arkham-asylum/34-31682/\">Arkham Asylum</a> as well as the characters <a href=\"/man-bat/29-6131/\">Man-Bat</a>, <a href=\"/ras-al-ghul/29-40816/\">Ra's al Ghul</a> and <a href=\"/talia-al-ghul/29-4917/\">Talia al Ghul</a>.</p><p>In September 2011, DC Comics relaunched their entire line in an event known as \"<a href=\"/the-new-52/12-56061/\">The New 52</a>\" which would see the premiere of fifty-two new titles, thus the long-running Batman series ended with #713 during <a href=\"/tony-daniel/26-4663/\">Tony S. Daniel</a>'s run on the title. In the new line, however, Tony Daniel will continue writing in the \"Batman universe\" but under the character's other main title, <a href=\"/detective-comics/49-18058/\">Detective Comics</a>. The new Batman series will be written instead by Scott Snyder (who was writing Detective Comics pre-relaunch).</p><h4>For Post <a href=\"/flashpoint/39-56280/\">Flashpoint</a> volume 2, refer to <a href=\"/batman/49-42721/\">Batman</a>.</h4><p><b>Trade Volumes that collect Batman issues:</b></p><p><b>Classic and Archived Batman:</b></p><ul><li><a href=\"../../the-batman-chronicles-volume-one/37-233426/\" rel=\"nofollow\">The Batman Chronicles Volume 1</a> (Batman #1, Detective Comics #27-37)</li><li><a href=\"../../the-batman-chronicles-volume-two/37-233429/\" rel=\"nofollow\">The Batman Chronicles Volume 2</a> (Batman #2-3, Detective Comics #39-45, New York World's Fair Comic #2)</li><li><a href=\"../../the-batman-chronicles-volume-three/37-233431/\" rel=\"nofollow\">The Batman Chronicles Volume 3</a> (Batman #4-5, Detective Comics #46-50, World's Finest #1)</li><li><a href=\"../../the-batman-chronicles-volume-four/37-233433/\" rel=\"nofollow\">The Batman Chronicles Volume 4</a> (Batman #6-7, Detective Comics #51-56, World's Finest #2-3)</li><li><a href=\"../../the-batman-chronicles-volume-five/37-233436/\" rel=\"nofollow\">The Batman Chronicles Volume 5</a> (Batman #8-9, Detective Comics #57-61, World's Finest #4)</li></ul><p><i>see also Batman Archives, Batman: The Dark Knight Archives, Batman: The Dynamic Duo Archives etc...</i></p><p><b>Modern Batman</b> (Post-Crisis)</p><ul><li><a href=\"/batman-year-one/49-18960/\">Batman: Year One</a> (Batman #404-407)</li><li><a href=\"/batman-the-many-deaths-of-batman/49-37883/\">Batman: The Many Deaths of Batman</a> (Batman #433-435)</li><li><a href=\"/batman-ten-nights-of-the-beast/49-34113/\">Batman: Ten Nights of the Beast</a> (#417-420)</li><li><a href=\"/batman-a-death-in-the-family/49-25187/\">Batman: A Death in the Family</a> (Batman #426-429)</li><li><a href=\"/robin-the-teen-wonder/49-38554/\">Robin: The Teen Wonder</a> (Batman #428 and 442, <span>Batman: Legends of the Dark Knight #100, Nightwing #101, Batman #428 and 442, Robin #126 and 132, and Teen Titans #29)</span></li><li><a href=\"/robin-a-hero-reborn/49-20003/\">Robin: A Hero Reborn</a> (Batman #455-457 and Robin #1-5)</li><li><a href=\"../../batman-knightfall-vol-1-broken-bat/37-217582/\" rel=\"nofollow\">Batman: Knightfall Part 1: Broken Bat</a> (Batman #491-497, Detective Comics #659-663)</li><li><a href=\"../../batman-knightfall-vol-2-who-rules-the-night/37-217584/\" rel=\"nofollow\">Batman: Knightfall Part 2: Who Rules the Night</a> (Batman #498-500, Detective Comics #664-666, Showcase '93 #7-8, Shadow of the Bat #16-18)</li><li><a href=\"../../batman-knightfall-vol-3-knightsend/37-217586/\" rel=\"nofollow\">Batman: Knightfall Part 3: Knightsend</a> (Batman #509-510)</li><li><a href=\"../../batman-no-mans-land-volume-1/37-118196/\" rel=\"nofollow\">Batman: No Man's Land Vol. 1</a> ( Batman #563-564, No Man's Land #1, Shadow of the Bat #83-84, Detective Comics #730-731, Legends of the Dark Knight #116)</li><li><a href=\"../../batman-no-mans-land-volume-2/37-131427/\" rel=\"nofollow\">Batman: No Man's Land Vol. 2</a> (Batman #565, Shadow of the Bat #85-87, Detective Comics #732-733, Legends of the Dark Knight #117, #119, Batman Chronicles #16)</li><li><a href=\"../../batman-no-mans-land-volume-three/37-131428/\" rel=\"nofollow\">Batman: No Man's Land Vol. 3</a> (Batman #566-569, Shadow of the Bat #88, Detective Comics #734-735, Legends of the Dark Knight #120-121)</li><li><a href=\"../../batman-no-mans-land-volume-four/37-131446/\" rel=\"nofollow\">Batman: No Man's Land Vol. 4</a> (Batman #571-572, Shadow of the Bat #92-93, Detective Comics #736, # 738-739, Legends of the Dark Knight #125, Batman Chronicles #18)</li><li><a href=\"../../batman-no-mans-land-volume-five/37-131454/\" rel=\"nofollow\">Batman: No Man's Land Vol. 5</a> (Batman #573-574, Shadow of the Bat #94, Detective Comics #740-741, Legends of the Dark Knight #126, No Man's Land #0)</li><li><a href=\"/batman-hush/49-27384/\">Batman: Hush</a> (Batman #608-619)</li><li><a href=\"/batman-broken-city-hardcover/49-32206/\">Batman: Broken City</a> (Batman #620-625)</li><li><a href=\"/batman-as-the-crow-flies/49-37505/\">Batman: As the Crow Flies</a> (Batman #626-630)</li><li><a href=\"../../batman-under-the-hood-/37-243679/\" rel=\"nofollow\">Batman: Under the Hood Volume 1</a> (Batman #635-641)</li><li><a href=\"../../batman-under-the-hood-/37-243680/\" rel=\"nofollow\">Batman: Under the Hood Volume 2</a> (Batman #645-650, Batman Annual #25)</li><li><a href=\"/batman-face-the-face/49-21970/\">Batman: Face the Face</a> (Batman #651-654, Detective Comics #817-820)</li><li><a href=\"/batman-batman-and-son/49-34112/\">Batman: Batman and Son</a> (Batman #655-658, #663-666)</li><li><a href=\"/batman-the-black-glove/49-28235/\">Batman: The Black Glove</a> (Batman #667-669, 672-675)</li><li><a href=\"/batman-the-resurrection-of-ras-al-ghul/49-31003/\">Batman: The Resurrection of Ra's al Ghul</a> (Batman #670-671, Batman Annual #26, Detective Comics #838-839, Robin Annual #7, Nightwing #138-139)</li><li><a href=\"/batman-rip/49-45155/\">Batman: Batman R.I.P.</a> (Batman #676-683)</li><li><a href=\"/batman-whatever-happened-to-the-caped-crusader/49-33186/\">Batman: Whatever Happened to the Caped Crusader</a> (Batman #686, Detective Comics #853, Secret Origins #36, Secret Origins Special #1, Batman: Black and White #2)</li><li><a href=\"/batman-long-shadows/49-34996/\">Batman: Long Shadows</a> (Batman #687-691)</li><li><a href=\"/batman-life-after-death/49-35013/\">Batman: Life After Death</a> (Batman #692-699)</li><li><a href=\"/batman-time-and-the-batman/49-39230/\">Batman: Time and the Batman</a> (Batman #700-703)</li></ul>");
                    model.get("count_of_issues").should.equal(714);
                    model.get("start_year").should.equal(1940);
                    model.get("publisher").should.be.instanceOf(db.collections.Publishers.model);
                    model.get("publisher").get("name").should.equal("DC Comics");
                    //@todo Test last-issue and first-issue
                }

            });
        });
    });
});