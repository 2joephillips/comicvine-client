var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function(key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/","")
        .replace("?format=json","")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/","_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function(err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function(){

    describe('Issues', function(){
        var collection = db.collections.Issues;

        it('should fetch the issues from the remote API', function(done) {
            collection.pageSize = 100;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should fetch the next page from the remote API', function(done) {
            collection.pageSize = 100;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should return custom page sizes', function(done) {
            collection.pageSize=10;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should return load next page of custom size', function(done) {
            collection.pageSize=10;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(10);

                done();
            }});
        });

        it('should create models for all the elements in the response', function() {
            collection.at(0).should.equal(collection.get(17));
            collection.at(9).should.equal(collection.get(33));
            //Not using "name" because is empty for lot if issues :(
            collection.pluck("issue_number").join(",").should.equal('13.00,24.00,57.00,115.00,16.00,3.00,107.00,109.00,13.00,52.00');
        });

        it('should expose issue attributes', function() {
            collection.pageSize = 100;
            collection.fetch({success: function(collection) {
                var issue = collection.get(16);

                issue.get("id").should.equal(16);
                issue.get("api_detail_url").should.equal("http://api.comicvine.com/issue/16/");
                issue.get("site_detail_url").should.equal("http://www.comicvine.com/journey-into-mystery-hands-off/37-16/");
                issue.get("description").should.equal("<h2>Four Mysterious stories in this issue: </h2><ul class=\"plain-list\"> <li><p><em>Revenge From Beyond</em></p> </li> <li><p><em>The corpse!</em></p> </li> <li><p><em>I didn't see a thing!</em></p> </li> <li><p><em>The stroke of midnight!</em></p> </li></ul>");
                issue.get("deck").should.equal("");
                issue.get("aliases").should.equal("");
                issue.get("date_last_updated").should.equal(new Date("2009-11-25 08:53:37"));
                issue.get("volume").should.be.instanceOf(db.collections.Volumes.model);
                issue.get("volume").get("name").should.equal("Journey Into Mystery");
                issue.get("issue_number").should.equal("3.00");
                issue.get("publish_date").should.equal(new Date("1952/10/01"));
                issue.get("date_added").should.equal(new Date("2008-06-06 11:10:16"));
                issue.get("name").should.equal("Hands Off!");
            }});
        });
    });
});