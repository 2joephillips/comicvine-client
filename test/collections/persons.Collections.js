var should = require("should"),
    backbone = require("backbone"),
    fs = require("fs"),
    request = require("request"),
    db = require("../../lib/db");

var SyncBuilder = require('../../lib/syncbuilder.js'),
    RequestBuilder = require("../../lib/requestbuilder.js"),
    UrlBuilder = require("../../lib/urlbuilder.js");

var fakeCache = function(key, ttl, fetch, callback) {
    var file = key
        .replace("http://example.com/","")
        .replace("?format=json","")
        .replace(/&offset=(\d+)&limit=(\d+)/, "$1-$2")
        .replace("/","_") + ".json";
    fs.readFile('test/mocks/' + file, 'utf8', function(err, data) {
        if (err) throw err;
        callback(null, JSON.parse(data));
    })
}
var url = new UrlBuilder("test_api_key", "http://example.com");
var req = new RequestBuilder(url, request);
var sync = new SyncBuilder(url, req, fakeCache);
backbone.sync = sync.build();

describe('db.Collections', function(){

    describe('Persons', function(){
        var collection = db.collections.Persons;

        it('should fetch the person from the remote API', function(done) {
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should fetch the next page from the remote API', function(done) {
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(100);
                done();
            }});
        });

        it('should return custom page sizes', function(done) {
            collection.pageSize=10;
            collection.fetch({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should return load next page of custom size', function(done) {
            collection.pageSize=10;
            collection.fetchNextPage({success: function(collection) {
                collection.length.should.equal(10);
                done();
            }});
        });

        it('should create models for all the elements in the response', function() {
            collection.at(0).should.equal(collection.get(1349));
            collection.at(9).should.equal(collection.get(1436));
            collection.pluck("name").join(",").should.equal('Patrick Woodroffe,Richard Corben,Charles Biro,Joe Benitez,Allan Emby,Rodolfo Migliari,Matt Milla,Phil Haxo,Adrian Moro,Steve Schanes');
        });

        it('should expose person attributes', function() {
            collection.fetch({success: function(collection) {
                collection.length.should.equal(100);
                var person = collection.get(1352);

                person.get("id").should.equal(1352);
                person.get("name").should.equal("Joe Benitez");
                person.get("aliases").should.equal("");
                person.get("api_detail_url").should.equal("http://api.comicvine.com/person/1352/");
                person.get("site_detail_url").should.equal("http://www.comicvine.com/joe-benitez/26-1352/");
                person.get("date_added").should.equal(new Date("2008-06-06 11:28:14"));
                person.get("date_last_updated").should.equal(new Date("2012-08-11 03:16:02"));
                person.get("birth").should.equal(new Date("1971-05-21"));
                person.get("hometown").should.equal("");
                person.get("count_of_issue_appearances").should.equal(200);
                person.get("email").should.equal("");
                person.get("country").should.equal("USA");
                person.get("deck").should.equal("Comic Book Artist");
                person.get("description").should.equal("<p>Joe Benitez has worked on some of the industries hottest titles; <a href=\"/the-darkness/29-24347/\">The Darkness</a>, <a href=\"/batman/29-1699/\">Batman</a>, <a href=\"/witchblade/29-18319/\">Witchblade</a>, <a href=\"/justice-league-of-america/65-31815/\">The Justice League of America</a>. and his own project <a href=\"/wraithborn/29-42674/\">Wraithborn</a>. He is also the co-creator of <a href=\"/weapon-zero/65-42950/\">Weapon Zero</a>.</p><h2><a href=\"/top-cow/57-606/\">Top Cow</a></h2><p><a href=\"/top-cow/57-606/\">Top Cow</a> Productions discovered him in 1993 at the <a href=\"/san-diego-comic-con/34-55786/\">San Diego Comicon</a> where <a href=\"/david-wohl/26-42523/\">David Wohl</a> liked his work and showed it to <a href=\"/marc-silvestri/26-15688/\">Marc Silvestri</a> he liked it and hired Joe. Working for Top Cow Productions, Joe has dished out a lot of good artwork.</p><h2><a href=\"/aspen-mlt/57-1862/\">Aspen</a></h2><div class=\"js-item-cage\" rel=\"image\" title=\"image\" id=\"2129248\"><div class=\"wiki-img-right\"><div class=\"wiki-img-medium\"><a href=\"http://media.comicvine.com/uploads/9/98528/2129248-lm1p05.jpg\" title=\"Original Art from Lady Mechanika #1\"><img id=\"2129248\" src=\"http://media.comicvine.com/uploads/9/98528/2129248-lm1p05_medium.jpg\" alt=\"Original Art from Lady Mechanika #1\" /></a><div class=\"item-caption\">Original Art from Lady Mechanika #1</div></div></div></div><p>While working for Aspen, Benitez wrote and drew his own series entitled, <a href=\"/lady-mechanika/49-35855/\">Lady Mechanika</a>. He stated that the Mega Con in Florida inspired some of the characters that he drew. There were a lot of people at this convention dressed in <a href=\"/steampunk/12-55983/\">Steampunk</a> inspired costumes. Benitez gained an admiration for this type of style that merged Victorian style clothing with high tech gear and gadgets.</p>");
                person.get("website").should.equal("http://wraithborn.com/");
            }});
        });
    });
});