module.exports = function(urlBuilder, request) {

    return {
        build: function (url) {
            return function (callback) {
                var urlWithAPI = urlBuilder.ensureApiKey(url);
                var options = {
                    url: urlWithAPI,
                    headers: {
                        'User-Agent': 'comicApp'
                    }
                };
                request(options, function (err, response, body) {
                    if (err) {
                        callback(err, null);
                    } else if (response.headers["content-type"] == "application/json; charset=utf-8") {
                        callback(null, JSON.parse(body));
                    } else {
                        callback(new Error("ComicVine API returned an invalid response"), null);
                    }
                });
            }
        }
    }
}
